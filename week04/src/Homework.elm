module Homework exposing
    ( User
    , decodeDate
    , decodeUser
    , jsonPair
    , mottos
    , mottosDecoder
    , jsonEncAccountInfo
    , jsonDecAccountInfo
    )

import Iso8601
import Json.Decode as J
import Json.Decode.Pipeline as P exposing (required)
import Json.Encode as Enc
import Time


type alias User =
    { name : String
    , cats : Maybe Int
    }


decodeUser : J.Decoder User
decodeUser =
    J.map2 (\name cats -> { name = name, cats = cats })
        (J.field "name" J.string)
        (J.field "cats" (J.nullable J.int))


mottos : String
mottos =
    """
      {"Germany": {"motto": "Einigkeit und Recht und Freiheit", "currency": "EUR"},
       "England": {"motto": "God Save the Queen", "currency": "GBP"},
       "France": {"motto": "Liberté, Égalité, Fraternité", "currency": "EUR"}}
      """


type alias Mottos =
    { countries : List Country }


type alias Country =
    { name : String
    , motto : String
    , currency : String
    }


mottosDecoder : J.Decoder Mottos
mottosDecoder =
    let
        mottoCurDec =
            J.map2 (\m c -> { motto = m, currency = c })
                (J.field "motto" J.string)
                (J.field "currency" J.string)

        toItem ( countryName, m ) =
            { name = countryName
            , motto = m.motto
            , currency = m.currency
            }
    in
    J.map (\vs -> { countries = List.map toItem vs })
        (J.keyValuePairs mottoCurDec)


decodeDate : J.Decoder Time.Posix
decodeDate =
    J.andThen
        (\s ->
            case Iso8601.toTime s of
                Err _ ->
                    J.fail "Failed to parse date"

                Ok p ->
                    J.succeed p
        )
        J.string


type alias AccountInfo =
    { id : Int
    , email : String
    , full_name : Maybe String
    , phone_number : Maybe String
    , info_complete : Bool
    }


jsonDecAccountInfo : J.Decoder AccountInfo
jsonDecAccountInfo =
    J.succeed (\pid pemail pfull_name pphone_number pinfo_complete -> { id = pid, email = pemail, full_name = pfull_name, phone_number = pphone_number, info_complete = pinfo_complete })
        |> required "id" J.int
        |> required "email" J.string
        |> P.optional "full_name" (J.map Just J.string) Nothing
        |> P.optional "phone_number" (J.map Just J.string) Nothing
        |> required "info_complete" J.bool



--
-- show students fnullable and actual jsonDecAccountInfo generated for meetup.events
--


maybeEncode : (a -> Enc.Value) -> Maybe a -> J.Value
maybeEncode f ma =
    case ma of
        Nothing ->
            Enc.null

        Just x ->
            f x


jsonEncAccountInfo : AccountInfo -> J.Value
jsonEncAccountInfo val =
    Enc.object
        [ ( "id", Enc.int val.id )
        , ( "email", Enc.string val.email )
        , ( "full_name", maybeEncode Enc.string val.full_name )
        , ( "phone_number", maybeEncode Enc.string val.phone_number )
        , ( "info_complete", Enc.bool val.info_complete )
        ]


jsonPair : J.Decoder a -> J.Decoder b -> J.Decoder ( a, b )
jsonPair a b =
    J.andThen
        (\xs ->
            case xs of
                [] ->
                    J.fail "Expecting a list of two elements"

                x :: y :: [] ->
                    case ( J.decodeValue a x, J.decodeValue b y ) of
                        ( Ok av, Ok bv ) ->
                            J.succeed ( av, bv )

                        _ ->
                            J.fail "Error while decoding individual pair fields"

                _ ->
                    J.fail "Expecting a list of two elements"
        )
        (J.list J.value)
