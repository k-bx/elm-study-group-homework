module MorseCode exposing (decode)

import Dict
import MorseCodes

decode : String -> String
decode code =
  let words = String.split "   " code
      decWord w =
        let letters = String.split " " w
            decLetter l =
              Dict.get l MorseCodes.get
        in String.concat (List.filterMap decLetter letters)
  in
  String.trim (String.concat (List.intersperse " " (List.map decWord words)))
