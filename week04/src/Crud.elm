module Crud exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as J
import Json.Decode.Pipeline exposing (required)
import Json.Encode
import List.Extra
import Maybe.Extra
import Platform.Sub
import Task
import Time


type alias UserInfo =
    { id : Int, name : String, surname : String }


jsonDecUserInfo : J.Decoder UserInfo
jsonDecUserInfo =
    J.succeed (\pid pname psurname -> { id = pid, name = pname, surname = psurname })
        |> required "id" J.int
        |> required "name" J.string
        |> required "surname" J.string


jsonEncCreateUserForm : { name : String, surname : String } -> J.Value
jsonEncCreateUserForm val =
    Json.Encode.object
        [ ( "name", Json.Encode.string val.name )
        , ( "surname", Json.Encode.string val.surname )
        ]


jsonEncUpdateUserForm =
    jsonEncCreateUserForm


type alias Model =
    { users : List UserInfo
    , prefix : String
    , name : String
    , surname : String
    , errors : List String
    , selectedUser : Maybe UserInfo
    }


type alias UserId =
    Int


type Msg
    = NoOp
    | UsersLoaded (Result Http.Error (List UserInfo))
    | FilterPrefixChange String
    | NameChanged String
    | SurnameChanged String
    | Create
    | UserCreated (Result Http.Error UserInfo)
    | SelectChanged String
    | Update
    | UpdateDone UserId (Result Http.Error UserInfo)
    | Delete
    | DeleteDone UserId (Result Http.Error ())


init : () -> ( Model, Cmd Msg )
init _ =
    ( { users = []
      , prefix = ""
      , name = ""
      , surname = ""
      , errors = []
      , selectedUser = Nothing
      }
    , loadUsers UsersLoaded
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        UsersLoaded (Err e) ->
            ( { model | errors = [ "Error while loading users" ] }, Cmd.none )

        UsersLoaded (Ok users) ->
            ( { model | users = users }, Cmd.none )

        FilterPrefixChange s ->
            ( { model | prefix = s }, Cmd.none )

        NameChanged s ->
            ( { model | name = s }, Cmd.none )

        SurnameChanged s ->
            ( { model | surname = s }, Cmd.none )

        Create ->
            ( model, createUser { name = model.name, surname = model.surname } UserCreated )

        UserCreated (Err e) ->
            ( { model | errors = [ "Error while creating a user" ] }, Cmd.none )

        UserCreated (Ok user) ->
            ( { model | users = model.users ++ [ user ] }, Cmd.none )

        SelectChanged s ->
            case List.Extra.find (\u -> renderUserName u == s) model.users of
                Nothing ->
                    ( model, Cmd.none )

                Just u ->
                    ( { model | name = u.name, surname = u.surname, selectedUser = Just u }, Cmd.none )

        Update ->
            case model.selectedUser of
                Nothing ->
                    ( model, Cmd.none )

                Just u ->
                    ( model, updateUser u.id { name = model.name, surname = model.surname } (UpdateDone u.id) )

        UpdateDone id (Err e) ->
            ( { model | errors = [ "Error while updating user" ] }, Cmd.none )

        UpdateDone id (Ok user) ->
            ( { model
                | users =
                    List.map
                        (\u ->
                            if u.id == id then
                                user

                            else
                                u
                        )
                        model.users
              }
            , Cmd.none
            )

        Delete ->
            case model.selectedUser of
                Nothing ->
                    ( model, Cmd.none )

                Just u ->
                    ( model, deleteUser u.id (DeleteDone u.id) )

        DeleteDone id (Err e) ->
            ( { model | errors = [ "Error while deleting user" ] }, Cmd.none )

        DeleteDone id (Ok ()) ->
            ( { model | users = List.filter (\u -> u.id /= id) model.users }, Cmd.none )


renderUserName u =
    u.name ++ " " ++ u.surname


filteredUsers prefix users =
    List.filter (\u -> String.startsWith prefix u.surname) users


view : Model -> Html Msg
view model =
    let
        renderUserOption u =
            option []
                [ text <| renderUserName u ]
    in
    div [ class "container mt-4" ]
        [ div [ class "form-group" ]
            [ text "Filter prefix: "
            , input
                [ type_ "text"
                , onInput FilterPrefixChange
                , value model.prefix
                ]
                []
            ]
        , div [ class "form-group" ]
            [ select
                [ class "form-control"
                , multiple True
                , onInput SelectChanged
                ]
                (List.map renderUserOption (filteredUsers model.prefix model.users))
            ]
        , div [ class "form-group" ]
            [ label []
                [ text "Name: "
                , input
                    [ type_ "text"
                    , onInput NameChanged
                    , value model.name
                    ]
                    []
                ]
            ]
        , div [ class "form-group" ]
            [ label []
                [ text "Surname: "
                , input
                    [ type_ "text"
                    , onInput SurnameChanged
                    , value model.surname
                    ]
                    []
                ]
            ]
        , div []
            [ button
                [ type_ "button"
                , class "btn btn-light"
                , onClick Create
                ]
                [ text "create"
                ]
            , button
                [ type_ "button"
                , class "btn btn-light"
                , onClick Update
                ]
                [ text "update"
                ]
            , button
                [ type_ "button"
                , class "btn btn-light"
                , onClick Delete
                ]
                [ text "delete"
                ]
            ]
        ]


loadUsers : (Result Http.Error (List UserInfo) -> msg) -> Cmd msg
loadUsers toMsg =
    Http.get
        { url = "http://localhost:8000/api/users/list.json"
        , expect = Http.expectJson toMsg (J.list jsonDecUserInfo)
        }


createUser : { name : String, surname : String } -> (Result Http.Error UserInfo -> msg) -> Cmd msg
createUser ps toMsg =
    Http.post
        { url = "http://localhost:8000/api/users/create.json"
        , expect = Http.expectJson toMsg jsonDecUserInfo
        , body = Http.jsonBody (jsonEncCreateUserForm ps)
        }


updateUser : Int -> { name : String, surname : String } -> (Result Http.Error UserInfo -> msg) -> Cmd msg
updateUser userId ps toMsg =
    Http.request
        { method = "PUT"
        , headers = []
        , url = "http://localhost:8000/api/users/" ++ String.fromInt userId ++ "/update.json"
        , body = Http.jsonBody (jsonEncUpdateUserForm ps)
        , expect = Http.expectJson toMsg jsonDecUserInfo
        , timeout = Nothing
        , tracker = Nothing
        }


deleteUser : Int -> (Result Http.Error () -> msg) -> Cmd msg
deleteUser userId toMsg =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = "http://localhost:8000/api/users/" ++ String.fromInt userId ++ "/delete.json"
        , body = Http.emptyBody
        , expect = Http.expectWhatever toMsg
        , timeout = Nothing
        , tracker = Nothing
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
