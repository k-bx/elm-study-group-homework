module Example exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, bool, int, list, string)
import Homework exposing (..)
import Json.Decode as J exposing (Decoder)
import Json.Encode as Enc
import Test exposing (..)


suite : Test
suite =
    describe "test homework"
        [ test "decode path from homework" <|
            \_ ->
                J.decodeString decodePath pathExample
                    |> Expect.equal (Ok (PathUrl { host = "http://example.com", port_ = 80 }))
        , fuzz3 bool string int "check that decoding encoded bath works via fuzz" <|
            \isFile fileOrHost port_ ->
                case isFile of
                    True ->
                        [ ( "path_type", Enc.string "file" )
                        , ( "value", Enc.string fileOrHost )
                        ]
                            |> Enc.object
                            |> J.decodeValue decodePath
                            |> Expect.equal (Ok (PathFile fileOrHost))

                    False ->
                        [ ( "path_type", Enc.string "url" )
                        , ( "value"
                          , Enc.object
                                [ ( "host", Enc.string fileOrHost )
                                , ( "port", Enc.int port_ )
                                ]
                          )
                        ]
                            |> Enc.object
                            |> J.decodeValue decodePath
                            |> Expect.equal (Ok (PathUrl { host = fileOrHost, port_ = port_ }))
        ]
