module GracefulLabeling exposing
    ( Graph
    , GraphLabeled
    , addInCache
    , displayGraph
    , edgeDiffs
    , emptyCache
    , fillVertex
    , fromCompleteGraph
    , g92
    , gWiki
    , gWikiAnswer
    , graceful
    , graphToLabeled
    , isInCache
    , isValid
    , run
    , tryLabel
    , unlabeled
    , validateEdgeDiffs
    )

import List.Extra
import Maybe.Extra


{-| List of label ids, and pairs which label is connected to which,
left <= right
-}
type alias Graph =
    { vertices : List Int, edges : List ( Int, Int ) }


type alias GraphLabeled =
    { vertices : List { origId : Int, label : Maybe Int }
    , edges : List ( Int, Int )
    , numEdges : Int
    }


graphToLabeled : Graph -> GraphLabeled
graphToLabeled graph =
    { edges = graph.edges
    , vertices = List.map (\v -> { origId = v, label = Nothing }) graph.vertices
    , numEdges = List.length graph.edges
    }


g92 : Graph
g92 =
    { vertices = List.range 1 14
    , edges =
        [ ( 1, 6 )
        , ( 2, 6 )
        , ( 3, 6 )
        , ( 4, 6 )
        , ( 5, 6 )
        , ( 5, 7 )
        , ( 5, 8 )
        , ( 8, 9 )
        , ( 5, 10 )
        , ( 10, 11 )
        , ( 11, 12 )
        , ( 11, 13 )
        , ( 13, 14 )
        ]
    }


{-|


## See [Graceful labeling](https://en.wikipedia.org/wiki/Graceful_labeling)

-- Looks same as solution but label with "id" 2 will be named "0"

-}
gWiki : Graph
gWiki =
    { vertices = [ 1, 2, 3, 4, 5 ]
    , edges =
        [ ( 1, 3 )
        , ( 2, 3 )
        , ( 2, 4 )
        , ( 2, 5 )
        , ( 4, 5 )
        ]
    }


gWikiAnswer : GraphLabeled
gWikiAnswer =
    { edges = [ ( 1, 3 ), ( 2, 3 ), ( 2, 4 ), ( 2, 5 ), ( 4, 5 ) ]
    , vertices =
        [ { label = Just 1, origId = 1 }
        , { label = Just 0, origId = 2 }
        , { label = Just 3, origId = 3 }
        , { label = Just 4, origId = 4 }
        , { label = Just 5, origId = 5 }
        ]
    , numEdges = 5
    }


graceful : Graph -> Maybe GraphLabeled
graceful g =
    case tryLabel (graphToLabeled g) 0 of
        [] ->
            Nothing

        x :: _ ->
            Just x


tryLabel : GraphLabeled -> Int -> List GraphLabeled
tryLabel g i =
    let
        unls =
            unlabeled g

        unlsLen =
            List.length unls
    in
    if getNumEdges g - i > unlsLen then
        []

    else if i > getNumEdges g then
        case unls of
            [] ->
                Maybe.Extra.toList (fromCompleteGraph g)

            _ ->
                []

    else
        case unls of
            [] ->
                case isValid g of
                    True ->
                        Maybe.Extra.toList (fromCompleteGraph g)

                    False ->
                        []

            xs ->
                concatMapFirst
                    (\x ->
                        let
                            filled =
                                fillVertex g x i
                        in
                        case isValid filled of
                            True ->
                                tryLabel filled (i + 1)

                            False ->
                                []
                    )
                    xs
                    ++ tryLabel g (i + 1)


{-| Like concatMap, but short-circuits |
-}
concatMapFirst : (a -> List b) -> List a -> List b
concatMapFirst f xss =
    case xss of
        [] ->
            []

        x :: xs ->
            case f x of
                [] ->
                    concatMapFirst f xs

                b :: _ ->
                    [ b ]


fromCompleteGraph : GraphLabeled -> Maybe GraphLabeled
fromCompleteGraph gl =
    let
        isAllJust =
            List.all (.label >> Maybe.Extra.isJust) gl.vertices

        convVertice v =
            v.origId
    in
    if isAllJust then
        Just gl
        -- { vertices = List.map convVertice gl.vertices
        -- , edges = gl.edges
        -- }

    else
        Nothing


unlabeled : GraphLabeled -> List Int
unlabeled gl =
    List.map .origId
        (List.filter (.label >> Maybe.Extra.isNothing) gl.vertices)


edgeDiffs gl =
    let
        edgeDiff ( a, b ) =
            let
                findLabelVal l =
                    case List.Extra.find (\x -> x.origId == l) gl.vertices of
                        Just v ->
                            v.label

                        Nothing ->
                            Nothing
            in
            case ( findLabelVal a, findLabelVal b ) of
                ( Just aVal, Just bVal ) ->
                    Just (abs (aVal - bVal))

                ( _, _ ) ->
                    Nothing
    in
    List.filterMap edgeDiff gl.edges


isValid : GraphLabeled -> Bool
isValid gl =
    validateEdgeDiffs emptyCache (edgeDiffs gl) (getNumEdges gl)


validateEdgeDiffs cache dss numEdges =
    case dss of
        [] ->
            True

        d :: ds ->
            if isInCache cache d then
                False

            else if d <= numEdges && d >= 0 then
                validateEdgeDiffs (addInCache cache d) ds numEdges

            else
                False


type alias Cache =
    Int -> Bool


emptyCache : Cache
emptyCache _ =
    False


isInCache : Cache -> Int -> Bool
isInCache f i =
    f i


addInCache : Cache -> Int -> Cache
addInCache f i j =
    if i == j then
        True

    else
        isInCache f j


fillVertex : GraphLabeled -> Int -> Int -> GraphLabeled
fillVertex gl x i =
    let
        newVertices =
            go gl.vertices

        go vss =
            case vss of
                [] ->
                    []

                v :: vs ->
                    if v.origId == x then
                        { v | label = Just i } :: vs

                    else
                        v :: go vs
    in
    { gl | vertices = newVertices }


getNumEdges : GraphLabeled -> Int
getNumEdges gl =
    gl.numEdges


displayGraph : GraphLabeled -> String
displayGraph x =
    let
        _ =
            Debug.log "" x
    in
    "todo"


run : String
run =
    case graceful gWiki of
        Just a ->
            displayGraph a

        Nothing ->
            "Alert the media! You have found and exception "
                ++ "of the Graceful Tree conjecture."
