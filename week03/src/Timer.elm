module Timer exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events exposing (..)
import Maybe.Extra
import Platform.Sub
import Task
import Time


type alias Model =
    { slider : Int
    , initialTime : Maybe Time.Posix
    , currTime : Maybe Time.Posix
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { slider = 10
      , initialTime = Nothing
      , currTime = Nothing
      }
    , Task.perform GotInitialTime Time.now
    )


type Msg
    = NoOp
    | SliderChange String
    | TimeUpdate Time.Posix
    | GotInitialTime Time.Posix
    | Reset


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SliderChange s ->
            case String.toInt s of
                Just v ->
                    ( { model | slider = v }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        GotInitialTime t ->
            ( { model | initialTime = Just t }, Cmd.none )

        TimeUpdate t ->
            let
                ctAndIt : ( Maybe Time.Posix, Maybe Time.Posix )
                ctAndIt =
                    Maybe.withDefault ( Just t, model.initialTime )
                        (Maybe.map2
                            (\c i ->
                                if Time.posixToMillis i + model.slider * 1000 <= Time.posixToMillis t then
                                    ( Just t, Just (Time.millisToPosix (Time.posixToMillis t - model.slider * 1000)) )

                                else
                                    ( Just t, Just i )
                            )
                            model.currTime
                            model.initialTime
                        )

                ( ct, it ) =
                    ctAndIt
            in
            ( { model | currTime = ct, initialTime = it }, Cmd.none )

        Reset ->
            ( { model | initialTime = Nothing, currTime = Nothing }
            , Task.perform GotInitialTime Time.now
            )


maybe : a -> (b -> a) -> Maybe b -> a
maybe a f mb =
    case mb of
        Nothing ->
            a

        Just b ->
            f b


view : Model -> Html Msg
view model =
    let
        elapsedTime =
            let
                percentPassed =
                    case ( model.initialTime, model.currTime ) of
                        ( Just i, Just c ) ->
                            round (toFloat (Time.posixToMillis c - Time.posixToMillis i) / (toFloat model.slider * 1000) * 100)

                        _ ->
                            0

                percentPassedStr =
                    String.fromInt percentPassed
            in
            div [ class "progress" ]
                [ div
                    [ class "progress-bar"
                    , attribute "role" "progressbar"
                    , style "width" (percentPassedStr ++ "%")
                    , attribute "aria-valuenow" percentPassedStr
                    , attribute "aria-valuemin" "0"
                    , attribute "aria-valuemax" "100"
                    ]
                    []
                ]
    in
    div [ class "container mt-4" ]
        [ div [ style "max-width" "20em" ]
            [ text "Elapsed time: "
            , elapsedTime
            ]
        , div []
            [ text <| String.fromInt model.slider ++ "s"
            ]
        , div []
            [ text "Duration: "
            , input
                [ type_ "range"
                , A.min "1"
                , A.max "100"
                , value (String.fromInt model.slider)
                , onInput SliderChange
                ]
                []
            ]
        , div []
            [ input
                [ type_ "button"
                , value "Reset"
                , onClick Reset
                ]
                []
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 200.0 TimeUpdate


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
