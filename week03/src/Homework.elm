module Homework exposing
    ( either
    , find
    , keepOks
    , mapOk
    , maybeToList
    , parseDate
    , updateList
    , updateListKv
    )

import Date exposing (Date)


maybeToList : Maybe a -> List a
maybeToList mx =
    case mx of
        Nothing ->
            []

        Just x ->
            [ x ]


updateList : (a -> Bool) -> (a -> Maybe a) -> List a -> List a
updateList shouldChange f xs =
    let
        upd x acc =
            if shouldChange x then
                case f x of
                    Nothing ->
                        acc

                    Just a2 ->
                        a2 :: acc

            else
                x :: acc
    in
    List.reverse (List.foldl upd [] xs)


find : (a -> Bool) -> List a -> Maybe a
find f xss =
    case xss of
        [] ->
            Nothing

        x :: xs ->
            if f x then
                Just x

            else
                find f xs


updateListKv :
    List ( k, v )
    -> k
    -> (v -> Maybe v)
    -> List ( k, v )
updateListKv xs k f =
    List.reverse <|
        List.foldl
            (\( k2, v2 ) acc ->
                if k2 == k then
                    case f v2 of
                        Just newV ->
                            ( k, newV ) :: acc

                        Nothing ->
                            acc

                else
                    ( k2, v2 ) :: acc
            )
            []
            xs


keepOks : List (Result a b) -> List b
keepOks xss =
    case xss of
        [] ->
            []

        x :: xs ->
            case x of
                Err _ ->
                    keepOks xs

                Ok v ->
                    v :: keepOks xs


mapOk : (b -> c) -> Result a b -> Result a c
mapOk f res =
    case res of
        Err e ->
            Err e

        Ok v ->
            Ok (f v)


either : (a -> c) -> (b -> c) -> Result a b -> c
either fa fb res =
    case res of
        Err a ->
            fa a

        Ok b ->
            fb b


{-| TODO: Show the pointfree Haskell converter
-}
parseDate : Maybe String -> Maybe Date
parseDate =
    Maybe.andThen
        (Date.fromIsoString
            >> either (always Nothing) (Just << identity)
        )
