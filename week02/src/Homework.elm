module Homework exposing
    ( bird
    , bird2
    , bird3
    , buildStatsUrl
    , catMaybes
    , convert
    , convert02Applicative
    , convert03
    , mapMaybes
    , setPhone
    )

import Date exposing (Date)
import Url.Builder as UrlBuilder



-- notice that I've omitted the argument


convert :
    List { name : String, email : String, phone_number : String }
    -> List { name : String, email : String }
convert =
    List.map (\x -> { name = x.name, email = x.email })


andMap : Maybe a -> Maybe (a -> b) -> Maybe b
andMap =
    Maybe.map2 (|>)


catMaybes : List (Maybe a) -> List a
catMaybes xs =
    case xs of
        [] ->
            []

        y :: ys ->
            case y of
                Nothing ->
                    catMaybes ys

                Just a ->
                    a :: catMaybes ys


mapMaybes : (a -> Maybe b) -> List a -> List b
mapMaybes f xs =
    case xs of
        [] ->
            []

        y :: ys ->
            case f y of
                Nothing ->
                    mapMaybes f ys

                Just z ->
                    z :: mapMaybes f ys



-- TODO: convert02Applicative more obvious (non-applicative)


convert02Applicative :
    List { name : Maybe String, email : Maybe String }
    -> List { name : String, email : String }
convert02Applicative xs =
    catMaybes
        (List.map
            (\x ->
                Just (\n e -> { name = n, email = e })
                    |> andMap x.name
                    |> andMap x.email
            )
            xs
        )


convert03 :
    List { name : Maybe String, email : Maybe String }
    -> List { name : String, email : String }
convert03 xs =
    catMaybes
        (List.map
            (\x ->
                Maybe.map
                    (\n ->
                        { name = n
                        , email = Maybe.withDefault "<unspecified>" x.email
                        }
                    )
                    x.name
            )
            xs
        )


bird : Int
bird =
    let
        notThree x =
            x /= 3

        incr x =
            x + 1
    in
    List.sum (List.filter notThree (List.map incr [ 1, 2, 3 ]))


bird2 : Int
bird2 =
    let
        notThree x =
            x /= 3

        incr x =
            x + 1
    in
    List.sum <|
        List.filter notThree <|
            List.map incr <|
                [ 1, 2, 3 ]


bird3 : Int
bird3 =
    let
        notThree x =
            x /= 3

        incr x =
            x + 1
    in
    [ 1, 2, 3 ]
        |> List.map incr
        |> List.filter notThree
        |> List.sum


type alias User =
    { profile : Profile }


type alias Profile =
    { address : Address }


type alias Address =
    { phone : String }


setPhone : String -> User -> User
setPhone p x =
    let
        addr =
            x.profile.address

        newAddr =
            { addr | phone = p }

        prof =
            x.profile

        newProf =
            { prof | address = newAddr }
    in
    { x | profile = newProf }


buildStatsUrl : Int -> { startDate : Maybe String, numElems : Maybe Int } -> String
buildStatsUrl itemId ps =
    "https://myapi.com"
        ++ UrlBuilder.absolute
            [ "api", "item", String.fromInt itemId, "stats.json" ]
            (catMaybes
                [ Maybe.map (UrlBuilder.string "start_date") ps.startDate
                , Maybe.map (UrlBuilder.string "num_items" << String.fromInt) ps.numElems
                ]
            )
