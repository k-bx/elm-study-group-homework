module TempConverter exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


type alias Model =
    { celsius : String
    , farenheit : String
    }


initialModel : Model
initialModel =
    { celsius = ""
    , farenheit = ""
    }


type Msg
    = CelsiusChange String
    | FarenheitChange String


update : Msg -> Model -> Model
update msg model =
    case msg of
        CelsiusChange s ->
            let
                m2 =
                    { model | celsius = s }
            in
            case String.toFloat s of
                Nothing ->
                    m2

                Just fl ->
                    { m2 | farenheit = String.fromFloat (celsiusToFarenheit fl) }

        FarenheitChange s ->
            let
                m2 =
                    { model | farenheit = s }
            in
            case String.toFloat s of
                Nothing ->
                    m2

                Just fl ->
                    { m2 | celsius = String.fromFloat (farenheitToCelsius fl) }


celsiusToFarenheit : Float -> Float
celsiusToFarenheit x =
    x * 9 / 5 + 32


farenheitToCelsius : Float -> Float
farenheitToCelsius x =
    (x - 32) * 5 / 9


view : Model -> Html Msg
view model =
    div []
        [ input
            [ type_ "text"
            , onInput CelsiusChange
            , value model.celsius
            ]
            []
        , text "Celsius = "
        , input
            [ type_ "text"
            , onInput FarenheitChange
            , value model.farenheit
            ]
            []
        ]


main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , view = view
        , update = update
        }
