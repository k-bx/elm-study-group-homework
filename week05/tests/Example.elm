module Example exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Homework exposing (..)
import Json.Decode as J exposing (Decoder)
import Test exposing (..)


suite : Test
suite =
    describe "test homework"
        [ test "decode path from homework" <|
            \_ ->
                J.decodeString decodePath pathExample
                    |> Expect.equal (Ok (PathUrl { host = "http://example.com", port_ = 80 }))
        ]
