module DatePickerSpa exposing
    ( Model
    , Msg(..)
    , init
    , main
    , subscriptions
    , update
    , view
    )

import Browser
import Date exposing (Date)
import DatePicker
import Html exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as J
import Json.Encode
import List.Extra
import Maybe.Extra
import Platform.Sub
import Task
import Time exposing (Weekday(..))


type alias Model =
    { errors : List String
    , date : Maybe Date
    , datePicker : DatePicker.DatePicker
    }


type Msg
    = NoOp
    | SetDatePicker DatePicker.Msg


init : () -> ( Model, Cmd Msg )
init _ =
    let
        ( datePicker, datePickerCmd ) =
            DatePicker.init
    in
    ( { errors = []
      , date = Nothing
      , datePicker = datePicker
      }
    , Cmd.none
    )


datePickerSettings =
    let
        def =
            DatePicker.defaultSettings
    in
    { def
        | inputClassList = [ ( "form-control", True ) ]
        , inputName = Just "date"
        , inputId = Just "date-field"
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SetDatePicker subMsg ->
            let
                ( newDatePicker, dateEvent ) =
                    DatePicker.update datePickerSettings subMsg model.datePicker

                date =
                    case dateEvent of
                        DatePicker.Picked newDate ->
                            Just newDate

                        _ ->
                            model.date
            in
            ( { model
                | date = date
                , datePicker = newDatePicker
              }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    let
        bornDay =
            case model.date of
                Nothing ->
                    "unknown"

                Just d ->
                    case Date.weekday d of
                        Mon ->
                            "Monday"

                        Tue ->
                            "Tuesday"

                        Wed ->
                            "Wednesday"

                        Thu ->
                            "Thursday"

                        Fri ->
                            "Friday"

                        Sat ->
                            "Saturday"

                        Sun ->
                            "Sunday"
    in
    div [ class "container mt-4" ]
        [ h1 [] [ text "Day of the Week You Were Born" ]
        , div []
            [ p [] [ text "Select your birth date:" ]
            , DatePicker.view
                model.date
                datePickerSettings
                model.datePicker
                |> Html.map SetDatePicker
            , p [ class "mt-2" ] [ text <| "Your were born on: " ++ bornDay ]
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
