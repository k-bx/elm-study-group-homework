module Homework exposing (Path(..), decodePath, pathExample)

import Json.Decode as J exposing (Decoder)


type Path
    = PathFile String
    | PathUrl { host : String, port_ : Int }


decHostAndPort : Decoder { host : String, port_ : Int }
decHostAndPort =
    J.map2 (\h p -> { host = h, port_ = p })
        (J.field "host" J.string)
        (J.field "port" J.int)


decodePath : Decoder Path
decodePath =
    J.field "path_type" J.string
        |> J.andThen
            (\ptype ->
                case ptype of
                    "file" ->
                        J.field "value" J.string
                            |> J.andThen
                                (\val ->
                                    J.succeed (PathFile val)
                                )

                    "url" ->
                        J.map PathUrl (J.field "value" decHostAndPort)

                    _ ->
                        J.fail "Type must be file or url"
            )


pathExample : String
pathExample =
    """
{"path_type": "url",
 "value": {"host": "http://example.com",
           "port": 80}}
  """

-- > import Json.Decode as J exposing (Decoder)
-- > J.decodeString decodePath pathExample
-- Ok (PathUrl { host = "http://example.com", port_ = 80 })
--     : Result J.Error Path
