module OwnJson exposing
    ( MyJsonDecoder
    , Val(..)
    , allNonNothing
    , decodeValue
    , keyValPairs
    , myJsonInt
    )


type Val
    = Str String
    | Num Int
    | Obj (List ( String, Val ))
    | Nil
    | Lst (List Val)


type MyJsonDecoder a
    = MyJsonDecoder (Val -> Result String a)


decodeValue : MyJsonDecoder a -> Val -> Result String a
decodeValue (MyJsonDecoder f) v =
    f v


myJsonInt : MyJsonDecoder Int
myJsonInt =
    MyJsonDecoder
        (\v ->
            case v of
                Num i ->
                    Ok i

                _ ->
                    Err "Wrong int type"
        )


allNonNothing : List (Maybe a) -> Result String (List a)
allNonNothing xs =
    case xs of
        [] ->
            Ok []

        Nothing :: _ ->
            Err "Nothing value found"

        (Just v) :: tail ->
            case allNonNothing tail of
                Err e ->
                    Err e

                Ok vs ->
                    Ok (v :: vs)


keyValPairs :
    MyJsonDecoder a
    -> MyJsonDecoder (List ( String, a ))
keyValPairs (MyJsonDecoder innerDec) =
    MyJsonDecoder
        (\v ->
            case v of
                Obj xs ->
                    let
                        -- decodeRight : (String, Val) -> Maybe (String, a)
                        decodeRight ( k, vl ) =
                            let
                                -- innerRes : Result String a
                                innerRes =
                                    innerDec vl
                            in
                            case innerRes of
                                Err _ ->
                                    Nothing

                                Ok innerVal ->
                                    Just ( k, innerVal )
                    in
                    allNonNothing (List.map decodeRight xs)

                _ ->
                    Err "Must be an object"
        )
