module Homework exposing
    ( clap
    , compress
    , dropEvery
    , elementAt
    , isPalindrome
    , myButLast
    , myLast
    , myLength
    , myReverse
    )


myLast : List Int -> Maybe Int
myLast xs =
    case xs of
        [] ->
            Nothing

        y :: [] ->
            Just y

        y :: ys ->
            myLast ys


myButLast : List Int -> Maybe Int
myButLast xs =
    case xs of
        [] ->
            Nothing

        y :: [] ->
            Nothing

        y :: z :: [] ->
            Just y

        y :: ys ->
            myButLast ys


elementAt : List Int -> Int -> Maybe Int
elementAt xs n =
    if n <= 1 then
        case xs of
            [] ->
                Nothing

            y :: _ ->
                Just y

    else
        case xs of
            [] ->
                Nothing

            y :: ys ->
                elementAt ys (n - 1)


myLength : List Int -> Int
myLength xs =
    case xs of
        [] ->
            0

        _ :: ys ->
            1 + myLength ys


myReverse : List a -> List a
myReverse xs =
    case xs of
        [] ->
            []

        y :: ys ->
            myReverse ys ++ [ y ]


isPalindrome : List Int -> Bool
isPalindrome xs =
    let
        go yl zl =
            case ( yl, zl ) of
                ( [], [] ) ->
                    True

                ( [], _ ) ->
                    False

                ( _, [] ) ->
                    False

                ( y :: ys, z :: zs ) ->
                    if y == z then
                        go ys zs

                    else
                        False
    in
    go xs (myReverse xs)


compress : String -> String
compress xs =
    let
        empty : Char -> Bool
        empty _ =
            False

        add : Char -> (Char -> Bool) -> (Char -> Bool)
        add x cache y =
            if x == y then
                True

            else
                cache y

        inCache : Char -> (Char -> Bool) -> Bool
        inCache c cache =
            cache c

        go : (Char -> Bool) -> List Char -> String
        go cache ys =
            case ys of
                [] ->
                    ""

                z :: zs ->
                    case inCache z cache of
                        True ->
                            go cache zs

                        False ->
                            let
                                newCache =
                                    add z cache
                            in
                            String.cons z (go newCache zs)
    in
    go empty (String.toList xs)


dropEvery : String -> Int -> String
dropEvery xs n =
    let
        go cs i =
            case cs of
                [] ->
                    []

                d :: ds ->
                    if n == i then
                        go ds 1

                    else
                        d :: go ds (i + 1)
    in
    String.fromList (go (String.toList xs) 1)


clap : String -> String
clap =
    String.concat << List.intersperse " 👏 " << String.words
